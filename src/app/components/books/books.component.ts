import { Component, OnInit } from '@angular/core';
import { BooksService } from './books.service';
import { Subject } from 'rxjs/Subject';
import { Book } from '../../models/book';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'books',
    templateUrl: './books.component.html',
    styleUrls: ['./books.component.scss'],
    providers: [ BooksService ]
})

export class BooksComponent implements OnInit {

    books: Book[];
    paginatedBooks: Book[];
    searchTerm$ = new Subject<string>();
    defaultPageNumber: number = 6;
    searchForm: FormGroup;
    
    constructor(private _service: BooksService, private fb: FormBuilder){
        
        this._service.search(this.searchTerm$).subscribe(books => {
            let term = this.searchTerm$.observers[0].destination.key;
            if(term.length >= 3 && term.length <= 12){
                this.books = books;
                this.getPageNumber({start: 0, end: this.defaultPageNumber});
            } else if(term.length === 0){
                this.paginatedBooks = [];
                this.books = [];
            }
        });

        this.searchForm = fb.group({
            term: [null, Validators.compose([Validators.minLength(3)])]
        })
    }

    ngOnInit(){
        // this._service.searchBooks('matematyka').subscribe(
        //     books => {
        //         this.books = books;
        //         this.getPageNumber({start: 0, end: this.defaultPageNumber});
        //     }
        // )
    }

    getPageNumber(page: any){
        this.paginatedBooks = this.books.slice(page.start, page.end);
    }

}