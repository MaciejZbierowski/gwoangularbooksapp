import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class BooksService {

    url: string = "https://gwo.pl/booksApi/v1/search?query=";

    constructor(private http: Http){}

    search(terms: Observable<string>) {
        return terms.debounceTime(300).distinctUntilChanged().switchMap(term => this.searchBooks(term));
    }

    searchBooks(term){
        return this.http.get(`${this.url}${encodeURIComponent(term)}`)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json() || 'Server output'));
    }

}