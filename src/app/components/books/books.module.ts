import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BooksComponent } from './books.component';
import { routing } from './books.routes';

@NgModule({
	imports: [
		CommonModule,
		routing,
	],
	declarations: [
		BooksComponent
	]
})

export class BooksModule {}