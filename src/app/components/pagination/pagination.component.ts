import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

@Component({
    selector: 'pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})

export class PaginationComponent {

    @Input() pageNumber: number;
    @Output() sendPageNumber = new EventEmitter<any>();
    pages: number[] = [];
    @Input() pageDefaultNumber: number;

    ngOnChanges(changes: SimpleChanges){
        this.pages = [];
        if(changes['pageNumber']){
            for(let i = 0; i < this.pageNumber/this.pageDefaultNumber; i++)
                this.pages.push(i+1);
        }
    }

    sendPage(page: any){
        this.sendPageNumber.emit({start: (page-1) * this.pageDefaultNumber, end: page*this.pageDefaultNumber})
    }

}